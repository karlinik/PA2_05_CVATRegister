#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <list>
#include <set>
#include <deque>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <memory>
using namespace std;

class CDate
{
public:
	//---------------------------------------------------------------------------------------------
	CDate(int               y,
		int               m,
		int               d)
		: m_Year(y),
		m_Month(m),
		m_Day(d)
	{
	}
	//---------------------------------------------------------------------------------------------
	int                      Compare(const CDate     & x) const
	{
		if (m_Year != x.m_Year)
			return m_Year - x.m_Year;
		if (m_Month != x.m_Month)
			return m_Month - x.m_Month;
		return m_Day - x.m_Day;
	}
	//---------------------------------------------------------------------------------------------
	int                      Year(void) const
	{
		return m_Year;
	}
	//---------------------------------------------------------------------------------------------
	int                      Month(void) const
	{
		return m_Month;
	}
	//---------------------------------------------------------------------------------------------
	int                      Day(void) const
	{
		return m_Day;
	}
	//---------------------------------------------------------------------------------------------
	friend ostream         & operator <<                   (ostream         & os,
		const CDate     & x)
	{
		char oldFill = os.fill();
		return os << setfill('0') << setw(4) << x.m_Year << "-"
			<< setw(2) << (int)x.m_Month << "-"
			<< setw(2) << (int)x.m_Day
			<< setfill(oldFill);
	}
	//---------------------------------------------------------------------------------------------
private:
	int16_t                  m_Year;
	int8_t                   m_Month;
	int8_t                   m_Day;
};
#endif /* __PROGTEST__ */

string Upper(string str) {
	string newStr = str;
	for (auto & c : newStr)
	{
		c = toupper(c);
	}
	return newStr;
}
/*-----------------------------------------------------------------*/
string StripUpper(string str) {
	string newStr = str;
	int i = 0;
	bool firstSpace = true;
	auto it = newStr.begin();
	while (it != newStr.end())
	{
		if ((*it == ' ') && i == 0)
			newStr.erase(newStr.begin());
		else if ((*it == ' ') && firstSpace)
		{
			firstSpace = false;
			it++;
			i++;
		}
		else if ((*it == ' ') && !firstSpace)
			newStr.erase(newStr.begin() + i);
		else
		{
			*it = toupper(*it);
			it++;
			i++;
			firstSpace = true;
		}
		if (it == newStr.end())
			break;
	}
	char it2 = newStr.back();
	if (it2 == ' ')
		newStr.pop_back();
	return newStr;
}
/*-----------------------------------------------------------------*/
class CInvoice
{
public:
	//Constructors
	CInvoice(const CDate     & date,
		const string    & seller,
		const string    & buyer,
		unsigned int      amount,
		double            VAT) :i_amount(amount), i_buyer(buyer), i_count(0),i_date(NULL), i_seller(seller), i_VAT(VAT)
	{
		i_date = new CDate((int)date.Year(), (int)date.Month(), (int)date.Day());
		i_upper_buyer = Upper(i_buyer);
		i_upper_seller = Upper(i_seller);
	}
	CInvoice(const CInvoice &other) {
		i_amount = other.i_amount;
		i_buyer = other.i_buyer;
		i_count = other.i_count;
		i_date = new CDate(other.i_date->Year(), other.i_date->Month(), other.i_date->Day());
		i_seller = other.i_seller;
		i_upper_buyer = other.i_upper_buyer;
		i_upper_seller = other.i_upper_seller;
		i_VAT = other.i_VAT;
	}
	~CInvoice() {
		delete i_date;
	}

	//operators
	bool					 operator==(const CInvoice &other) const;
	bool					 operator!=(const CInvoice &other) const;
	bool					 operator<(const CInvoice &other) const;
	friend ostream&			 operator<<(ostream &os, CInvoice&x);
	CInvoice&				 operator =(const CInvoice &other);

	//getters
	CDate                    Date(void) const { return *i_date; }
	string                   Buyer(void) const { return i_buyer; }
	string                   Seller(void) const { return i_seller; }
	string                   UpperBuyer(void) const { return i_upper_buyer; }
	string                   UpperSeller(void) const { return i_upper_seller; }
	int                      Amount(void) const { return i_amount; }
	double                   VAT(void) const { return i_VAT; }
	int&					 SetCount(void) { return i_count; }
	int						 Count(void)const { return i_count; }


private:
	unsigned int i_amount;
	string i_buyer;
	int i_count;
	CDate* i_date;
	string i_seller;
	string i_upper_buyer;
	string i_upper_seller;
	double i_VAT;
};
/*-----------------------------------------------------------------*/
bool CInvoice::operator==(const CInvoice &other) const {
	if ((this->i_amount == other.i_amount) &&
		(this->i_buyer == other.i_buyer) &&
		(this->i_date->Year() == other.i_date->Year()) &&
		(this->i_date->Month() == other.i_date->Month()) &&
		(this->i_date->Day() == other.i_date->Day()) &&
		(this->i_seller == other.i_seller) &&
		(this->i_VAT == other.i_VAT))
		return 1;
	else
		return 0;
}
/*-----------------------------------------------------------------*/
bool CInvoice::operator!=(const CInvoice &other) const {
	return !(*this == other);
}
/*-----------------------------------------------------------------*/
bool CInvoice::operator<(const CInvoice &other) const {
	if (this->i_buyer < other.i_buyer) return true;
	if (this->i_buyer == other.i_buyer && this->i_seller < other.i_seller) return true;
	if (this->i_buyer == other.i_buyer && this->i_seller == other.i_seller && this->i_amount < other.i_amount) return true;
	if (this->i_buyer == other.i_buyer && this->i_seller == other.i_seller && this->i_amount == other.i_amount && this->i_VAT < other.i_VAT) return true;
	if (this->i_buyer == other.i_buyer && this->i_seller == other.i_seller && this->i_amount == other.i_amount && this->i_VAT == other.i_VAT && !(this->i_date->Compare(*(other.i_date)))) return true;
	return false;
}
/*-----------------------------------------------------------------*/
ostream& operator<<(ostream &os, CInvoice&x) {
	return os << x.Date() << " " << x.Buyer() << " " << x.Seller() << " " << x.Amount() << " " << x.VAT();
}
/*-----------------------------------------------------------------*/
CInvoice& CInvoice::operator =(const CInvoice &other) {
	if (&other == this) return *this;
	delete i_date;

	i_amount = other.i_amount;
	i_buyer = other.i_buyer;
	i_date = new CDate(other.i_date->Year(), other.i_date->Month(), other.i_date->Day());
	i_seller = other.i_seller;
	i_VAT = other.i_VAT;
	i_count = other.i_count;
	return *this;
}
/*-----------------------------------------------------------------*/

class CSortOpt
{
public:
	static const int         BY_DATE = 0;
	static const int         BY_BUYER = 1;
	static const int         BY_SELLER = 2;
	static const int         BY_AMOUNT = 3;
	static const int         BY_VAT = 4;
	CSortOpt(void) {}
	CSortOpt & AddKey(int key, bool ascending = true) {
		s_key_list.push_back(key);
		s_order_list.push_back(!ascending);
		return *this;
	}
	bool operator()(const CInvoice &lhs, const CInvoice &rhs) {
		//string lhs_upper_seller = Upper(lhs.Seller());
		//string lhs_upper_buyer = Upper(lhs.Buyer());
		//string rhs_upper_seller = Upper(rhs.Seller());
		//string rhs_upper_buyer = Upper(rhs.Buyer());
		size_t i = 0;
		while (i<s_key_list.size())
		{
			switch (s_key_list[i])
			{
			default:
			case BY_DATE:
			{
				int comp = lhs.Date().Compare(rhs.Date());
				if (comp != 0)
				{
					if (comp < 0) return 1 ^ s_order_list[i];
					return 0 ^ s_order_list[i];
				}
				break;
			}
			case BY_BUYER:
				if (lhs.UpperBuyer() != rhs.UpperBuyer())
					return ((lhs.UpperBuyer() < rhs.UpperBuyer()) ^ s_order_list[i]);
				break;
			case BY_SELLER:
				if (lhs.UpperSeller() != rhs.UpperSeller())
					return ((lhs.UpperSeller() < rhs.UpperSeller()) ^ s_order_list[i]);
				break;
			case BY_AMOUNT:
				if (lhs.Amount() != rhs.Amount())
					return ((lhs.Amount() < rhs.Amount()) ^ s_order_list[i]);
				break;
			case BY_VAT:
				if (lhs.VAT() != rhs.VAT())
					return ((lhs.VAT() < rhs.VAT()) ^ s_order_list[i]);
				break;
			}
			i++;
		}
		return lhs.Count()<rhs.Count();
	}
	/*void PrintSort() const{
	cout << "key size:" << s_key_list.size() << endl;
	for (auto it = s_key_list.begin(); it != s_key_list.end(); it++)
	cout << (*it) << " ";
	cout << endl;
	for (auto it2 = s_order_list.begin(); it2 != s_order_list.end(); it2++)
	cout << (*it2) << " ";
	cout << endl;
	}*/
private:
	vector<bool>s_order_list;
	vector<int>s_key_list;
};

struct CCompany {
	//member variables
	string m_name;
	string m_upper_name;
	//constructor
	CCompany(const string &name, const string &upper_name) :m_name(name), m_upper_name(upper_name) {}
	bool Compare(const CCompany *lhs, const string &rhs) {
		return (lhs->m_upper_name < rhs);
	}
};

/*void PrintList(const list<CInvoice> &list) {
for (auto it = list.begin(); it != list.end(); it++)
{
cout << (*it).Date() << " " << (*it).Seller() << " " << (*it).Buyer() << " " << (*it).Amount() << " " << (*it).VAT() << endl;
}
}*/

struct InvoiceHasher {
	size_t operator()(const CInvoice &in) const {
		string tmp = in.Seller()
			+ in.Buyer()
			+ to_string(in.Date().Year())
			+ to_string(in.Date().Month())
			+ to_string(in.Date().Day())
			+ to_string(in.Amount())
			+ to_string(in.VAT());
		return (hash<string>()(tmp));
	}
};

class CVATRegister
{
public:
	CVATRegister(void) {} //inicializace prazdne instance registru
	~CVATRegister(void) {
		for (auto it2 = m_invoices_issued.begin(); it2 != m_invoices_issued.end(); it2++)
		{
			delete it2->second;
		}
		for (auto it1 = m_invoices_accepted.begin(); it1 != m_invoices_accepted.end(); it1++)
		{
			delete it1->second;
		}

		for (auto i = m_companies.begin(); i != m_companies.end(); i++)
			delete *i;
	}
	bool                     RegisterCompany(const string    & name); //case insensitive
	bool                     AddIssued(const CInvoice  & x); //vola firma, ktera fakturu vydala
	bool                     AddAccepted(const CInvoice  & x); //...
	bool                     DelIssued(const CInvoice  & x);
	bool                     DelAccepted(const CInvoice  & x);
	list<CInvoice>           Unmatched(const string    & company,
		const CSortOpt  & sortBy) const;
	static bool Compare(const CCompany* lhs, const string &rhs) {
		return (lhs->m_upper_name < rhs);
	}
	//void PrintRegister();
	int BinarySearch(string &name) const;
private:
	vector<CCompany*>m_companies;
	unordered_map<CInvoice, CInvoice*, InvoiceHasher>m_invoices_accepted;
	unordered_map<CInvoice, CInvoice*, InvoiceHasher>m_invoices_issued;
	int m_count = 0;
};
/*-------------------------------------------------------------------------------*/
int CVATRegister::BinarySearch(string &name) const {
	int mid;
	int min = 0, max = m_companies.size() - 1;
	while (max >= min) {
		mid = (min + max) / 2;

		if (m_companies[mid]->m_upper_name < name)
			min = mid + 1;
		else if (m_companies[mid]->m_upper_name > name)
			max = mid - 1;
		else
			return mid;
	}
	return -1;
}
/*-------------------------------------------------------------------------------*/
bool CVATRegister::RegisterCompany(const string  & name) {
	string newName = StripUpper(name);
	auto it = lower_bound(m_companies.begin(), m_companies.end(), newName, &CVATRegister::Compare);

	if ((it != m_companies.end()) && (*it)->m_upper_name == newName) //nalezena stejna jiz registrovana firma
		return false;
	CCompany *company = new CCompany(name, newName);
	m_companies.insert(it, company);
	return true;
}
/*-------------------------------------------------------------------------------*/
bool CVATRegister::AddIssued(const CInvoice  & x) { //vydal seller	

	string buyer = StripUpper(x.Buyer());
	string seller = StripUpper(x.Seller());

	//jestli nejsou stejne firmy
	if (buyer == seller)
		return false;

	//zkontrolovat, zda jsou firmy registrovane
	int it1 = BinarySearch(buyer);
	int it2 = BinarySearch(seller);

	if (it1 < 0 || it2 < 0) //vracene zaporne cislo -> nenalezeno
		return false;

	CInvoice invoice(x.Date(), m_companies[it2]->m_name, m_companies[it1]->m_name, x.Amount(), x.VAT());

	//kontrola, jestli faktura uz neexistuje 
	auto it = m_invoices_issued.find(invoice);
	if ((it != m_invoices_issued.end())) //nalezena stejna jiz registrovana firma
		return false;

	//pridani do map
	invoice.SetCount() = m_count++;
	m_invoices_issued.insert(make_pair(invoice, nullptr));
	auto itaccepted = m_invoices_accepted.find(invoice);
	if (itaccepted != m_invoices_accepted.end())
	{
		CInvoice* newInvoice = new CInvoice(x.Date(), m_companies[it2]->m_name, m_companies[it1]->m_name, x.Amount(), x.VAT());
		auto itissued = m_invoices_issued.find(invoice);
		itissued->second = newInvoice;
		itaccepted->second = newInvoice;
	}
	return true;
}
/*-------------------------------------------------------------------------------*/
bool CVATRegister::AddAccepted(const CInvoice  & x) {

	string buyer = StripUpper(x.Buyer());
	string seller = StripUpper(x.Seller());

	//jestli nejsou stejne firmy
	if (buyer == seller)
		return false;

	//zkontrolovat, zda jsou firmy registrovane
	int it1 = BinarySearch(buyer);
	int it2 = BinarySearch(seller);

	if (it1 < 0 || it2 < 0) //vracene zaporne cislo -> nenalezeno
		return false;

	CInvoice invoice(x.Date(), m_companies[it2]->m_name, m_companies[it1]->m_name, x.Amount(), x.VAT());

	//kontrola, jestli faktura uz neexistuje 
	auto it = m_invoices_accepted.find(invoice);
	if ((it != m_invoices_accepted.end())) //nalezena stejna jiz registrovana firma
		return false;

	//pridani do map
	invoice.SetCount() = m_count++;
	m_invoices_accepted.insert(make_pair(invoice, nullptr));
	auto itissued= m_invoices_issued.find(invoice);
	if (itissued != m_invoices_issued.end())
	{
		CInvoice* newInvoice = new CInvoice(x.Date(), m_companies[it2]->m_name, m_companies[it1]->m_name, x.Amount(), x.VAT());
		auto itaccepted = m_invoices_accepted.find(invoice);
		itaccepted->second = newInvoice;
		itissued->second = newInvoice;
	}
	
	return true;
}
/*-------------------------------------------------------------------------------*/
bool CVATRegister::DelIssued(const CInvoice  & x) {
	string buyer = StripUpper(x.Buyer());
	string seller = StripUpper(x.Seller());

	//zkontrolovat, zda jsou firmy registrovane
	int it1 = BinarySearch(buyer);
	int it2 = BinarySearch(seller);

	if (it1<0 || it2<0) //vracene zaporne cislo -> nenalezeno
		return false;

	CInvoice invoice(x.Date(), m_companies[it2]->m_name, m_companies[it1]->m_name, x.Amount(), x.VAT());

	auto it = m_invoices_issued.find(invoice);
	if (it == m_invoices_issued.end())
		return false;
	if ((*it).second)
	{
		auto it2 = m_invoices_accepted.find(invoice);
		it2->second = NULL;
	}
	m_invoices_issued.erase(it);
	return true;
	//uvolnim tim pamet? Mozna memleak???
}
/*-------------------------------------------------------------------------------*/
bool CVATRegister::DelAccepted(const CInvoice  & x) {

	string buyer = StripUpper(x.Buyer());
	string seller = StripUpper(x.Seller());

	//zkontrolovat, zda jsou firmy registrovane
	int it1 = BinarySearch(buyer);
	int it2 = BinarySearch(seller);

	if (it1<0 || it2<0) //vracene zaporne cislo -> nenalezeno
		return false;

	CInvoice invoice(x.Date(), m_companies[it2]->m_name, m_companies[it1]->m_name, x.Amount(), x.VAT());

	auto it = m_invoices_accepted.find(invoice);
	if (it == m_invoices_accepted.end())
		return false;
	if ((*it).second)
	{
		auto it2 = m_invoices_issued.find(invoice);
		it2->second = NULL;
	}
	m_invoices_accepted.erase(it);
	return true;
	//uvolnim tim pamet? Mozna memleak???
}
/*-------------------------------------------------------------------------------*/
list<CInvoice> CVATRegister::Unmatched(const string & company, const CSortOpt & sortBy) const {

	string upper_company = StripUpper(company);
	list<CInvoice>list;
	int it1 = BinarySearch(upper_company);
	if (it1 < 0) return list;
	for (auto it = m_invoices_issued.begin(); it != m_invoices_issued.end(); it++)
	{
		if (((*it).first.Seller() == m_companies[it1]->m_name || (*it).first.Buyer() == m_companies[it1]->m_name) && !(*it).second)
		{
			list.push_back((*it).first);
		}
	}
	for (auto it = m_invoices_accepted.begin(); it != m_invoices_accepted.end(); it++)
	{
		if (((*it).first.Seller() == m_companies[it1]->m_name || (*it).first.Buyer() == m_companies[it1]->m_name) && !(*it).second)
			list.push_back((*it).first);
	}

	list.sort(sortBy);
	return list;
}
/*-------------------------------------------------------------------------------*/
/*void CVATRegister::PrintRegister() {
cout << setw(40)<<"******" << endl;
cout << "Companies:" << endl;
for (auto it = m_companies.begin(); it != m_companies.end(); it++)
{
cout <<setw(25) <<(*it)->m_name << " = " << (*it)->m_upper_name << endl;
}
cout << "Invoices accepted:" << endl;
for (auto it = m_invoices_accepted.begin(); it != m_invoices_accepted.end(); it++)
{
cout << (*it).first.Date()<< " | " << (*it).first.Seller() <<" | " << (*it).first.Buyer() << " | " << (*it).first.Amount() << " | " << (*it).first.VAT() << " || "<<(*it).first.Count()<<endl;
}
cout << "Invoices issued:" << endl;
for (auto it = m_invoices_issued.begin(); it != m_invoices_issued.end(); it++)
{
cout << (*it).first.Date() << " | " << (*it).first.Seller() << " | " << (*it).first.Buyer() << " | " << (*it).first.Amount() << " | " << (*it).first.VAT() << " || " << (*it).first.Count()<< endl;
}
cout << setw(40) << "******" << endl;
cout << m_count << endl;
}*/
/*-------------------------------------------------------------------------------*/


#ifndef __PROGTEST__
bool equalLists(const list<CInvoice> & a, const list<CInvoice> & b)
{
	if (a.size() != b.size()) return false;
	if (a.empty() && b.empty()) return true;

	auto it1 = a.begin();
	auto it2 = b.begin();

	while (it1 != a.end() && it2 != b.end())
	{
		if ((*it1) != (*it2))
			return 0;
		it1++;
		it2++;
	}
	return 1;
}

int main(void)
{
	CVATRegister r;
	assert(r.RegisterCompany("first Company"));
	assert(r.RegisterCompany("Second     Company"));
	assert(r.RegisterCompany("ThirdCompany, Ltd."));
	assert(r.RegisterCompany("Third Company, Ltd."));
	assert(!r.RegisterCompany("Third Company, Ltd."));
	assert(!r.RegisterCompany(" Third  Company,  Ltd.  "));
	assert(r.AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company ", 100, 20)));
	assert(r.AddIssued(CInvoice(CDate(2000, 1, 2), "FirSt Company", "Second Company ", 200, 30)));
	assert(r.AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company ", 100, 30)));
	assert(r.AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company ", 300, 30)));
	assert(r.AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", " Third  Company,  Ltd.   ", 200, 30)));
	assert(r.AddIssued(CInvoice(CDate(2000, 1, 1), "Second Company ", "First Company", 300, 30)));
	assert(r.AddIssued(CInvoice(CDate(2000, 1, 1), "Third  Company,  Ltd.", "  Second    COMPANY ", 400, 34)));
	assert(!r.AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company ", 300, 30)));
	assert(!r.AddIssued(CInvoice(CDate(2000, 1, 4), "First Company", "First   Company", 200, 30)));
	assert(!r.AddIssued(CInvoice(CDate(2000, 1, 4), "Another Company", "First   Company", 200, 30)));
	//r.PrintRegister();
	//r.Unmatched("First Company", CSortOpt().AddKey(CSortOpt::BY_SELLER, true).AddKey(CSortOpt::BY_BUYER, false).AddKey(CSortOpt::BY_DATE, false));
	assert(equalLists(r.Unmatched("First Company", CSortOpt().AddKey(CSortOpt::BY_SELLER, true).AddKey(CSortOpt::BY_BUYER, false).AddKey(CSortOpt::BY_DATE, false)),
		list<CInvoice>
	{
		CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
			CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000)
	}));
	assert(equalLists(r.Unmatched("First Company", CSortOpt().AddKey(CSortOpt::BY_DATE, true).AddKey(CSortOpt::BY_SELLER, true).AddKey(CSortOpt::BY_BUYER, true)),
		list<CInvoice>
	{
		CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
			CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000)
	}));
	assert(equalLists(r.Unmatched("First Company", CSortOpt().AddKey(CSortOpt::BY_VAT, true).AddKey(CSortOpt::BY_AMOUNT, true).AddKey(CSortOpt::BY_DATE, true).AddKey(CSortOpt::BY_SELLER, true).AddKey(CSortOpt::BY_BUYER, true)),
		list<CInvoice>
	{
		CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
			CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000)
	}));
	assert(equalLists(r.Unmatched("First Company", CSortOpt()),
		list<CInvoice>
	{
		CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
			CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
			CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000)
	}));
	assert(equalLists(r.Unmatched("second company", CSortOpt().AddKey(CSortOpt::BY_DATE, false)),
		list<CInvoice>
	{
		CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 1), "Third Company, Ltd.", "Second     Company", 400, 34.000000)
	}));
	assert(equalLists(r.Unmatched("last company", CSortOpt().AddKey(CSortOpt::BY_VAT, true)),
		list<CInvoice>
	{
	}));
	assert(r.AddAccepted(CInvoice(CDate(2000, 1, 2), "First Company", "Second Company ", 200, 30)));
	assert(r.AddAccepted(CInvoice(CDate(2000, 1, 1), "First Company", " Third  Company,  Ltd.   ", 200, 30)));
	assert(r.AddAccepted(CInvoice(CDate(2000, 1, 1), "Second company ", "First Company", 300, 32)));
	assert(equalLists(r.Unmatched("First Company", CSortOpt().AddKey(CSortOpt::BY_SELLER, true).AddKey(CSortOpt::BY_BUYER, true).AddKey(CSortOpt::BY_DATE, true)),
		list<CInvoice>
	{
		CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 32.000000)
	}));
	assert(!r.DelIssued(CInvoice(CDate(2001, 1, 1), "First Company", "Second Company ", 200, 30)));
	assert(!r.DelIssued(CInvoice(CDate(2000, 1, 1), "A First Company", "Second Company ", 200, 30)));
	assert(!r.DelIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Hand", 200, 30)));
	assert(!r.DelIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company", 1200, 30)));
	assert(!r.DelIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company", 200, 130)));
	assert(r.DelIssued(CInvoice(CDate(2000, 1, 2), "First Company", "Second Company", 200, 30)));
	assert(equalLists(r.Unmatched("First Company", CSortOpt().AddKey(CSortOpt::BY_SELLER, true).AddKey(CSortOpt::BY_BUYER, true).AddKey(CSortOpt::BY_DATE, true)),
		list<CInvoice>
	{
		CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
			CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 32.000000)
	}));
	assert(r.DelAccepted(CInvoice(CDate(2000, 1, 1), "First Company", " Third  Company,  Ltd.   ", 200, 30)));
	assert(equalLists(r.Unmatched("First Company", CSortOpt().AddKey(CSortOpt::BY_SELLER, true).AddKey(CSortOpt::BY_BUYER, true).AddKey(CSortOpt::BY_DATE, true)),
		list<CInvoice>
	{
		CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
			CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 32.000000)
	}));
	assert(r.DelIssued(CInvoice(CDate(2000, 1, 1), "First Company", " Third  Company,  Ltd.   ", 200, 30)));
	assert(equalLists(r.Unmatched("First Company", CSortOpt().AddKey(CSortOpt::BY_SELLER, true).AddKey(CSortOpt::BY_BUYER, true).AddKey(CSortOpt::BY_DATE, true)),
		list<CInvoice>
	{
		CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
			CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
			CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
			CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 32.000000)
	}));

	//system("pause");
	return 0;
}
#endif /* __PROGTEST__ */
